import React, { Component } from "react";
import { nanoid } from "nanoid";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };

  addTodo = (ev) => {
    let newState = {};

    if (ev.key === "Enter" && ev.target.value !== "") {
      newState.todos = [...this.state.todos];
      let arr = newState.todos[newState.todos.length - 1];
      newState.todos.push({
        completed: false,
        id: arr ? arr.id + 1 : 1,
        title: ev.target.value,
        userId: 1,
      });

      this.setState(newState);

      ev.target.value = "";
      ev.target.placeholder = "What needs to be done?";
    } else if (ev.target.value === "") {
      ev.target.placeholder = "Oops, you forgot to type something!";
    }
  };

  // better way to do this?
  clearCompleted = (ev) => {
    let newState = {};

    newState.todos = this.state.todos.filter((x) => !x.completed);

    this.setState(newState);
  };

  // better way to do this?
  toggleComplete = (todoId) => {
    let newState = {};

    newState.todos = this.state.todos;
    newState.todos[todoId - 1].completed = !newState.todos[todoId - 1]
      .completed;

    this.setState(newState);
  };

  // Jake, if you read this comment can you Slack me with a better way to do the following? My toggleComplete function depends on the id's of each todo item
  // and my delete button was breaking it b/c there would be a gap (if I deleted some in the middle, id's would look like 1, 2, 3, 4, 10, 11 etc)
  deleteTodo = (id) => {
    let newState = this.state.todos.filter((todo) => todo.id !== id);

    newState.map((x) => (x.id = 1));
    newState.map((x, i) => (x.id += i));

    this.setState({ todos: newState });
  };

  render() {
    const { todos } = this.state;
    let num = 0;
    todos.map((x) => (!x.completed ? num++ : null));

    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyDown={this.addTodo}
          />
        </header>

        <TodoList
          todos={this.state.todos}
          deleteTodo={this.deleteTodo}
          toggleComplete={this.toggleComplete}
        />

        <footer className="footer">
          <span className="todo-count">
            <strong>{num}</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearCompleted}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li
        id={this.props.id}
        className={this.props.completed ? "completed" : ""}
      >
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={() => this.props.toggleComplete(this.props.id)}
          />
          <label>{this.props.title}</label>
          <button
            className="destroy"
            onClick={() => this.props.deleteTodo(this.props.id)}
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              key={nanoid()}
              id={todo.id}
              deleteTodo={this.props.deleteTodo}
              toggleComplete={this.props.toggleComplete}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
